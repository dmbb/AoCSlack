#!/bin/bash

while getopts s:l:d: flag
do
    case "${flag}" in
        s) server=${OPTARG};;
        d) directory=${OPTARG};;
    esac
done
echo "Updating config in server and directory $server:$directory";

echo "Transferring files to $server:$directory..."
scp  config.json root@$server:$directory