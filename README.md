# Advent of Code Slack App

Create a JSON file named with the Leaderboard ID (the number at the end of the leaderboard, Session cookie, test webhook and real webhook, and optionally admin name if you want to be updated when the session expires.

```json
{
  "admin": "",
  "session": "",
  "webhook": "",
  "testWebhook": "",
  "leaderboard": ""
}
```

Launches with the test webhook by default.

Use `-live` to launch the live version.

Use `-cfg` to specify the path to the config file

# Deploying and Stopping
`./deploy.sh` and `./stop.sh` are both used for deploying to a remote server.
* `-s` server IP
* `-d` directory, default is where I keep it
* `-l` start it live

# Wants/Todo
* Automatic scaling down of star checker when out of season
* Support for slash commands / old years (needs an open server to accept requests from slack).