package main

import (
	"flag"
	log "github.com/sirupsen/logrus"
	"gitlab.com/dmbb/AoCSlack/advent"
	"gitlab.com/dmbb/AoCSlack/cfg"
	"gitlab.com/dmbb/AoCSlack/monitor"
	"gitlab.com/dmbb/AoCSlack/slack"
	"net/http"
	"os"
	"path/filepath"
	"time"
)

var exPath = ""

func init() {
	ex, _ := os.Executable()
	exPath = filepath.Dir(ex)

	file, err := os.OpenFile(exPath+"/output.log", os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err == nil {
		log.SetOutput(file)
	} else {
		log.Info("Failed to log to file")
	}

	log.SetLevel(log.InfoLevel)
}

const starUpdateInterval = 5 * time.Minute

func main() {
	live := flag.Bool("live", false, "Publish to the live webhook.")
	configFile := flag.String("cfg", "config.json", "Config file path")
	flag.Parse()

	if *live {
		log.Info("We're live!")
	} else {
		log.Info("Running in Test mode.")
	}

	config, err := cfg.New(*configFile, *live, exPath)
	if err != nil {
		log.Errorf("Failed to create config file: %v", err)
		panic(err)
	}

	client := &http.Client{}
	sc := slack.NewClient(client, config)
	ac := advent.NewClient(client, config, sc)

	starMonitor, starChan := monitor.NewStar(config, sc, ac)
	starMonitor.Start()

	dayMonitor, dayChan := monitor.NewDay(config, sc, ac)
	dayMonitor.Start()

	select {
	case <-starChan:
		return
	case <-dayChan:
		return
	}
}
