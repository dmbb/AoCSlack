#!/bin/bash

directory=/root/projects/AoCSlack

while getopts s:l:d: flag
do
    case "${flag}" in
        s) server=${OPTARG};;
        d) directory=${OPTARG};;
        l) live=true;;
    esac
done
echo "Deploying to server and directory $server:$directory";

echo "Building AoCSlack..."
env GOOS=linux GOARCH=amd64 go build -v .
if [ $? -ne 0 ]; then
  exit
fi

./stop.sh -s $server

echo "Transferring files to $server:$directory..."
scp AoCSlack config.json root@$server:$directory

echo "Starting AoCSlack..."
if [[ $live ]]; then
	echo "Starting AoCSlack"
	ssh root@$server "$directory/AoCSlack -cfg $directory/config.json -live </dev/null >/dev/null 2>&1 &"
	ssh root@$server 'ps axf | grep AoCSlack | grep -v grep'
else
	echo "Starting AoCSlack in test mode"
	ssh root@$server "$directory/AoCSlack -cfg $directory/config.json </dev/null >/dev/null 2>&1 &"
	ssh root@$server 'ps axf | grep AoCSlack | grep -v grep'
fi
