module gitlab.com/dmbb/AoCSlack

go 1.15

require (
	github.com/cenkalti/backoff/v4 v4.1.0
	github.com/fsnotify/fsnotify v1.4.9
	github.com/jinzhu/copier v0.3.2
	github.com/microcosm-cc/bluemonday v1.0.4
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b
)
