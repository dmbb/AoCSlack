package slack

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/dmbb/AoCSlack/cfg"
)

type Client interface {
	Send(b *Block) error
	SendText(s string) error
}

func NewClient(httpClient *http.Client, config cfg.Config) Client {
	return &client{httpClient: httpClient, config: config}
}

type client struct {
	httpClient *http.Client
	config     cfg.Config
}

func (c *client) Send(b *Block) error {
	data, err := json.Marshal(b)
	if err != nil {
		return fmt.Errorf("failed to marshal block: %v", err)
	}

	var req *http.Request
	req, err = http.NewRequest("POST", c.config.GetWebhook(), bytes.NewBuffer(data))
	if err != nil {
		return fmt.Errorf("failed to create POST request: %v", err)
	}

	req.Header.Set("Content-Type", "application/json")
	resp, err := c.httpClient.Do(req)
	if err != nil {
		return fmt.Errorf("failed to send POST request: %v", err)
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected status code: %v", resp.StatusCode)
	}

	return nil
}

func (c *client) SendText(s string) error {
	b := NewBlock()
	b.AddTextBlock(s)

	data, err := json.Marshal(b)
	if err != nil {
		return fmt.Errorf("failed to marshal block: %v", err)
	}

	var req *http.Request
	req, err = http.NewRequest("POST", c.config.GetWebhook(), bytes.NewBuffer(data))
	if err != nil {
		return fmt.Errorf("failed to create POST request: %v", err)
	}

	req.Header.Set("Content-Type", "application/json")
	resp, err := c.httpClient.Do(req)
	if err != nil {
		return fmt.Errorf("failed to send POST request: %v", err)
	}
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("unexpected status code: %v", resp.StatusCode)
	}

	return nil
}
