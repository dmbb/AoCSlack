package slack

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/dmbb/AoCSlack/lb"
)

type Block struct {
	Blocks []section `json:"blocks"`
}

type section struct {
	Type string `json:"type"`
	Text text   `json:"text"`
}

type text struct {
	Text string `json:"text"`
	Type string `json:"type"`
}

func NewBlock() *Block {
	return &Block{}
}

func (b *Block) AddTextBlock(s string) {
	sect := section{
		Type: "section",
		Text: text{
			Text: s,
			Type: "mrkdwn",
		},
	}
	b.Blocks = append(b.Blocks, sect)
}

func (b *Block) AddURLBlock(s, url string) {
	b.AddTextBlock(fmt.Sprintf("<%v|%v>", url, s))
}

func (b *Block) AddURL(url string) {
	b.AddURLBlock("View Leaderboard", strings.TrimSuffix(url, ".json"))
}

const nothing = " "
const part1 = "-"
const part2 = "*"

func addDailyStars(sb *strings.Builder, member lb.Member) {
	for i := 1; i <= 25; i++ {
		day := strconv.Itoa(i)

		// Nothing completed
		if member.Completion[day] == nil || member.Completion[day]["1"] == (lb.StarTimestamp{}) {
			sb.WriteString(nothing)
			continue
		}

		if member.Completion[day]["2"] == (lb.StarTimestamp{}) {
			// Part 1 complete
			sb.WriteString(part1)
			continue
		}

		// Part 2 complete
		sb.WriteString(part2)

	}
}

const noLastStar = "   %v	  N/A\n"
const lastStar = "   %v%v%v %v at %v:%02v:%02v\n"

func addLatestStarBlurb(sb *strings.Builder, member lb.Member) {
	t := lb.ParseTimestamp(member.LastStarTS)
	if t == time.Unix(0, 0) {
		sb.WriteString(fmt.Sprintf(noLastStar, member.LocalScore))
		return
	}
	_, month, d := t.Date()
	h, m, s := t.Clock()

	space := "	"
	if member.LocalScore < 100 {
		space += " "
	}
	if member.LocalScore < 10 {
		space += " "
	}

	sb.WriteString(fmt.Sprintf(lastStar, member.LocalScore, space, month.String(), d, h, m, s))
}

const tableHeader = "                           1111111111222222\n" + "                  1234567890123456789012345\n"
const nameFormat = "%15.15s   "
const codeBlock = "```"

func (b *Block) AddLeaderboardBlock(members []lb.Member) {
	var sb strings.Builder

	sb.WriteString(codeBlock)
	sb.WriteString(tableHeader)
	for _, member := range members {
		sb.WriteString(fmt.Sprintf(nameFormat, member.Name))

		addDailyStars(&sb, member)
		addLatestStarBlurb(&sb, member)
	}
	sb.WriteString(codeBlock)
	b.AddTextBlock(sb.String())
}

const leaderboardHeader = ":christmas_tree:Current lb as of %v:%02v on %v %v:christmas_tree:\n<http://adventofcode.com/2017/lb/private/view/129487|View Leaderboard>\n"

func (b *Block) AddLeaderboardHeaderBlock() {
	current := time.Now()
	_, m, d := current.Date()
	h, min, _ := current.Clock()
	s := fmt.Sprintf(leaderboardHeader, h, min, m, d)
	b.AddTextBlock(s)
}
