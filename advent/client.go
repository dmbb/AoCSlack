package advent

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/microcosm-cc/bluemonday"
	log "github.com/sirupsen/logrus"
	"gitlab.com/dmbb/AoCSlack/cfg"
	"gitlab.com/dmbb/AoCSlack/lb"
	"gitlab.com/dmbb/AoCSlack/slack"
	"golang.org/x/net/html"
)

type Client interface {
	GetDay(day, year int) (title, content, url string, err error)
	GetLeaderboard(year int) (*lb.Leaderboard, error)
}

func NewClient(client *http.Client, config cfg.Config, sc slack.Client) Client {
	a := &advent{client: client, config: config, sc: sc}
	a.policy = bluemonday.StripTagsPolicy()
	a.cookie = true
	return a
}

type advent struct {
	client *http.Client
	config cfg.Config
	policy *bluemonday.Policy
	sc     slack.Client
	cookie bool
}

func renderNode(n *html.Node) (string, error) {
	var buf bytes.Buffer
	w := io.Writer(&buf)
	err := html.Render(w, n)
	if err != nil {
		return "", err
	}
	return buf.String(), nil
}

func (a *advent) parseDayHTML(doc *html.Node) (title, content string, err error) {
	var article *html.Node
	var crawler func(*html.Node)

	crawler = func(node *html.Node) {
		if node.Type == html.ElementNode && node.Data == "article" {
			article = node
			return
		}
		for child := node.FirstChild; child != nil; child = child.NextSibling {
			crawler(child)
		}
	}
	crawler(doc)

	if article != nil {
		title, err = renderNode(article.FirstChild.FirstChild)
		if err != nil {
			return
		}
		sb := &strings.Builder{}
		for node := article.FirstChild.NextSibling; node != nil; node = node.NextSibling {
			content, err = renderNode(node)
			if err != nil {
				return
			}
			if strings.Contains(content, "<pre><code>") {
				sb.WriteString("```")
				sb.WriteString(a.policy.Sanitize(content))
				sb.WriteString("```")
			} else {
				if sb.Len() != 0 {
					sb.WriteString("\n")
				}
				sb.WriteString("> ")
				if strings.Contains(content, "<code>") {
					content = strings.ReplaceAll(content, "<code>", "`")
					content = strings.ReplaceAll(content, "</code>", "`")
				}
				if strings.Contains(content, "<em") {
					content = strings.ReplaceAll(content, "<em class=\"star\">", "_*")
					content = strings.ReplaceAll(content, "<em>", "_*")
					content = strings.ReplaceAll(content, "</em>", "*_")
				}
				if strings.Contains(content, "`_*") {
					content = strings.ReplaceAll(content, "`_*", "_*`")
					content = strings.ReplaceAll(content, "*_`", "`*_")
				}
				if sb.Len()+len(a.policy.Sanitize(content)) > 2000 {
					sb.WriteString("...")
					break
				}
				sb.WriteString(a.policy.Sanitize(content))
			}
		}
		content = sb.String()
		return
	}

	return "", "", fmt.Errorf("missing <article> in the node tree")
}

const dayURL = "https://adventofcode.com/%v/day/%v"

func (a *advent) GetDay(day, year int) (title, content, url string, err error) {
	url = fmt.Sprintf(dayURL, year, day)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		err = fmt.Errorf("failed to create request: %v", err)
		return
	}

	resp, err := a.client.Do(req)
	if err != nil {
		err = fmt.Errorf("failed to fetch day %v page: %v", day, err)
		return
	}
	if resp.StatusCode != http.StatusOK {
		err = fmt.Errorf("unexpected status code: %v", resp.StatusCode)
		return
	}

	doc, err := html.Parse(resp.Body)
	if err != nil {
		err = fmt.Errorf("failed to parse day %v html: %v", day, err)
		return
	}

	title, content, err = a.parseDayHTML(doc)
	if err != nil {
		err = fmt.Errorf("failed to parse day %v html: %v", day, err)
		return
	}

	log.Info(title, content)
	return
}

func (a *advent) GetLeaderboard(year int) (*lb.Leaderboard, error) {
	req, err := http.NewRequest("GET", a.config.GetLeaderboardUrlJson(year), nil)
	l := lb.New()
	if err != nil {
		return l, err
	}

	req.AddCookie(&http.Cookie{Name: "session", Value: a.config.GetSession()})
	resp, err := a.client.Do(req)
	if err != nil {
		return l, err
	}
	if resp.StatusCode != http.StatusOK {
		return l, fmt.Errorf("url %v unexpected status code: %v", req.URL, resp.StatusCode)
	}

	newUrl := resp.Request.URL.String()
	if newUrl != req.URL.String() {
		if a.cookie {
			a.cookie = false
			er := a.sc.SendText(fmt.Sprintf("%s No valid session token -- pausing updates.", a.config.GetAdmin()))
			if er != nil {
				log.Errorf("Failed to send cookie request: %v", er)
			}
		}
		return l, fmt.Errorf("no valid cookie, request was redirected")
	}
	if !a.cookie {
		err := a.sc.SendText(fmt.Sprintf("Detected a valid session token -- resuming updates."))
		if err != nil {
			log.Errorf("Failed to send cookie request: %v", err)
		}
		a.cookie = true
	}

	err = l.Decode(resp)
	if err != nil {
		return l, err
	}

	return l, nil
}
