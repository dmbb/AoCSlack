#!/bin/bash

while getopts s:l: flag
do
    case "${flag}" in
        s) server=${OPTARG};;
    esac
done
echo "Stopping AoCSlack on $server..."
ssh root@$server 'pkill -f AoCSlack && exit'
echo "AoCSlack stopped"
