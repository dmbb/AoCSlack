package cfg

import (
	"encoding/json"
	"fmt"
	"github.com/fsnotify/fsnotify"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"sync"
)

type Config interface {
	GetAdmin() string
	GetExPath() string
	GetSession() string
	GetWebhook() string
	GetTestWebhook() string
	GetLeaderboardUrl(year int) string
	GetLeaderboardUrlJson(year int) string
}

func New(file string, live bool, exPath string) (Config, error) {
	var err error
	c := &config{file: file, live: live, exPath: exPath}

	c.watcher, err = fsnotify.NewWatcher()
	if err != nil {
		return c, err
	}
	go c.watchFile()

	err = c.loadFile()

	return c, err
}

type config struct {
	sync.Mutex

	Admin string `json:"admin"`
	Session string `json:"session"`
	Webhook string `json:"webhook"`
	TestWebhook string `json:"testWebhook"`
	Leaderboard string `json:"leaderboard"`

	live bool
	file string
	exPath string

	watcher *fsnotify.Watcher
}

func (c *config) loadFile() error {
	c.Lock()
	defer c.Unlock()
	content, err := ioutil.ReadFile(c.file)
	if err != nil {
		return err
	}

	err = json.Unmarshal(content, c)
	if err != nil {
		return err
	}

	log.Infof("Successfully loaded config file: %v", c.file)
	return nil
}

func (c *config) watchFile() {
	err := c.watcher.Add(c.file)
	if err != nil {
		log.Errorf("Failed to watch file: %v", err)
		return
	}
	log.Infof("Watching file %v", c.file)

	for {
		select {
		case event, ok := <-c.watcher.Events:
			if !ok {
				return
			}
			if event.Op&fsnotify.Write == fsnotify.Write {
				log.Infof("Detected modified file: %v", event.Name)
				err := c.loadFile()
				if err != nil {
					log.Errorf("Failed to load file: %v", err)
				}
			}
		case err, ok := <-c.watcher.Errors:
			if !ok {
				return
			}
			log.Errorf("Error while watching file: %v", err)
		}
	}
}

func (c *config) GetAdmin() string {
	c.Lock()
	defer c.Unlock()

	return c.Admin
}

func (c *config) GetSession() string {
	c.Lock()
	defer c.Unlock()

	return c.Session
}

func (c *config) GetWebhook() string {
	c.Lock()
	defer c.Unlock()

	if c.live {
		return c.Webhook
	} else {
		return c.TestWebhook
	}
}

func (c *config) GetTestWebhook() string {
	c.Lock()
	defer c.Unlock()

	return c.TestWebhook
}

const leaderboardUrlFormat = "https://adventofcode.com/%d/leaderboard/private/view/%s"

func (c *config) GetLeaderboardUrl(year int) string {
	c.Lock()
	defer c.Unlock()

	return fmt.Sprintf(leaderboardUrlFormat, year, c.Leaderboard)
}

const leaderboardUrlJsonFormat = "https://adventofcode.com/%d/leaderboard/private/view/%s.json"

func (c *config) GetLeaderboardUrlJson(year int) string {
	c.Lock()
	defer c.Unlock()

	return fmt.Sprintf(leaderboardUrlJsonFormat, year, c.Leaderboard)
}

func (c *config) GetExPath() string {
	c.Lock()
	defer c.Unlock()

	return c.exPath
}
