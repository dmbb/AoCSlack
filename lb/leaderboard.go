package lb

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/jinzhu/copier"
	log "github.com/sirupsen/logrus"
)

// Leaderboard holds the full representation of the lb json
type Leaderboard struct {
	Members map[string]Member `json:"members"`
}

func New() *Leaderboard {
	return &Leaderboard{}
}

func (l *Leaderboard) Decode(r *http.Response) error {
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(l)
	return err
}

const leaderboardFile = "/lb.json"

func (l *Leaderboard) Load(path string) error {
	content, err := ioutil.ReadFile(path + leaderboardFile)
	if err != nil {
		return err
	}

	err = json.Unmarshal(content, l)
	if err != nil {
		return err
	}

	log.Infof("Successfully loaded leaderboard file: %v", path+leaderboardFile)
	return nil
}

func (l *Leaderboard) Save(path string) error {
	data, err := json.MarshalIndent(l, "", "")
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(path+leaderboardFile, data, 0644)
	if err != nil {
		return err
	}

	log.Infof("Successfully saved leaderboard file: %v", path+leaderboardFile)
	return nil
}

func (l *Leaderboard) GetDifference(old *Leaderboard) []Member {
	//This is so dumb, but it's the easiest deep copy i could do.
	newLb := &Leaderboard{}
	data, _ := json.MarshalIndent(l, "", "")
	json.Unmarshal(data, newLb)
	updated := make([]Member, 0, 0)
	for k, v := range newLb.Members {
		if _, ok := old.Members[k]; !ok || v.Stars > old.Members[k].Stars {
			diff := Member{}
			copier.Copy(&diff, &v)
			for day, starMap := range old.Members[k].Completion {
				if len(starMap) == 2 || len(starMap) == len(diff.Completion[day]) {
					delete(diff.Completion, day)
				} else {
					delete(diff.Completion[day], "1")
				}
			}
			updated = append(updated, diff)
		}
	}
	if len(updated) > 0 {
		log.Infof("Found differences: %v", updated)
	} else {
		log.Infof("Found no differences")
	}
	return updated
}

func (l *Leaderboard) GetMembers() []Member {
	members := make([]Member, 0, 0)
	for _, m := range l.Members {
		members = sortedInsert(members, m)
	}
	return members
}

func sortedInsert(slice []Member, new Member) []Member {
	l := len(slice)
	if l == 0 {
		return append(slice, new)
	}
	for i, check := range slice {
		if new.LocalScore > check.LocalScore {
			if i == 0 {
				return append([]Member{new}, slice...)
			}
			b := make([]Member, len(slice))
			copy(b, slice)
			b = append(b[0:i], new)
			return append(b, slice[i:]...)
		}
	}
	return append(slice, new)
}
