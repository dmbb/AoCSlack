package lb

import (
	"reflect"
	"time"
)

// StarTimestamp holds the individual star timestamps located within the map of map
type StarTimestamp struct {
	GetStarTS interface{} `json:"get_star_ts,string"`
}

func ParseTimestamp(ts interface{}) time.Time {
	var t time.Time
	if reflect.TypeOf(ts).Kind() == reflect.Float64 {
		t = time.Unix(int64(int(ts.(float64))), 0)
	} else {
		t = time.Unix(0, 0)
	}

	location, _ := time.LoadLocation("America/New_York")
	t.In(location)
	return t
}
