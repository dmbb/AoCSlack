package lb

import "time"

// Member holds the specific member information.
type Member struct {
	ID          string                              `json:"id"`
	Name        string                              `json:"name"`
	GlobalScore int                                 `json:"global_score"`
	Stars       int                                 `json:"stars"`
	LocalScore  int                                 `json:"local_score"`
	LastStarTS  interface{}                         `json:"last_star_ts,string"`
	Completion  map[string]map[string]StarTimestamp `json:"completion_day_level"`
}

func (m *Member) FindLastStar() (day, star string, ts time.Time) {
	for k, v := range m.Completion {
		for star, ts := range v {
			if m.LastStarTS.(string) == ts.GetStarTS.(string) {
				return k, star, ParseTimestamp(m.LastStarTS)
			}
		}
	}
	return "", "", time.Time{}
}

func (m *Member) GetStarOrder() []CompletionKey {
	starOrder := make([]CompletionKey, 0)

	for day, starMap := range m.Completion {
		for star := range starMap {
			starOrder = m.sortedStarInsert(starOrder, CompletionKey{Day: day, Star: star})
		}
	}

	return starOrder
}

type CompletionKey struct {
	Day string
	Star string
}

func (m *Member) sortedStarInsert(slice []CompletionKey, n CompletionKey) []CompletionKey {
	l := len(slice)
	if l == 0 {
		return append(slice, n)
	}
	for i, check := range slice {
		if ParseTimestamp(m.Completion[n.Day][n.Star]).After(ParseTimestamp(m.Completion[check.Day][check.Star])) {
			if i == 0 {
				return append([]CompletionKey{n}, slice...)
			}
			b := make([]CompletionKey, len(slice))
			copy(b, slice)
			b = append(b[0:i], n)
			return append(b, slice[i:]...)
		}
	}
	return append(slice, n)
}