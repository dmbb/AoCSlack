package monitor

import (
	"fmt"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/dmbb/AoCSlack/advent"
	"gitlab.com/dmbb/AoCSlack/cfg"
	"gitlab.com/dmbb/AoCSlack/lb"
	"gitlab.com/dmbb/AoCSlack/slack"
)

type Star interface {
	Start()
	Stop()
}

func NewStar(config cfg.Config, sc slack.Client, ac advent.Client) (Star, chan struct{}) {
	s := &star{config: config, sc: sc, ac: ac}
	s.quit = make(chan struct{})
	return s, make(chan struct{})
}

type star struct {
	config   cfg.Config
	sc       slack.Client
	ac       advent.Client
	interval time.Duration

	quit chan struct{}
}

const starCompleteText = "%s %s completed Day %s Star %s at %s! %s"
const starEmoji = ":star:"
const star2Emoji = ":star2:"

func (s *star) sendStarComplete(m lb.Member, cur *lb.Leaderboard) error {
	b := slack.NewBlock()

	log.Info(m.GetStarOrder())
	for _, completionKey := range m.GetStarOrder() {
		emoji := starEmoji
		if completionKey.Star == "2" {
			emoji = star2Emoji
		}
		t := lb.ParseTimestamp(m.Completion[completionKey.Day][completionKey.Star].GetStarTS)
		str := fmt.Sprintf(starCompleteText, emoji, m.Name, completionKey.Day, completionKey.Star, t.Format("15:04:05"), emoji)
		log.Infof(str)
		b.AddTextBlock(str)
	}
	b.AddLeaderboardBlock([]lb.Member{cur.Members[m.ID]})
	log.Infof("Sending update: %v", b)
	err := s.sc.Send(b)
	if err != nil {
		return fmt.Errorf("failed to send star complete: %v", err)
	}

	return nil
}

func (s *star) checkStarStatus(old *lb.Leaderboard, year int) *lb.Leaderboard {
	cur, err := s.ac.GetLeaderboard(year)
	if err != nil {
		log.Errorf("failed to fetch leaderboard: %s", err.Error())
		return old
	}

	if len(old.Members) == 0 {
		log.Info("No old leaderboard found, assuming no new updates")
		return cur
	}

	for _, m := range cur.GetDifference(old) {
		err = s.sendStarComplete(m, cur)
		if err != nil {
			log.Errorf("failed to send notifications: %s", err.Error())
			return old
		}
	}

	return cur
}

const decemberInterval = 5 * time.Minute
const offseasonInterval = 4 * time.Hour

func (s *star) monitor() {
	s.interval = decemberInterval

	t := time.NewTicker(s.interval)
	for {
		now := time.Now()
		year := time.Now().Year()
		if now.Month() != time.December && now.Month() != time.November {
			year--
		}

		l := lb.New()
		err := l.Load(s.config.GetExPath())
		if err != nil {
			log.Errorf("failed to load leaderboard: %v", err)
		}
		l = s.checkStarStatus(l, year)
		err = l.Save(s.config.GetExPath())
		if err != nil {
			log.Errorf("failed to save leaderboard: %v", err)
		}

		if now.Month() == time.December || (now.Month() == time.November && now.Day() >= 30) {
			if s.interval != decemberInterval {
				s.interval = decemberInterval
				t.Reset(s.interval)
				log.Infof("Using December interval of %v", s.interval)
			}
		} else {
			if s.interval != offseasonInterval {
				s.interval = offseasonInterval
				t.Reset(s.interval)
				log.Infof("Using off-season interval of %v", s.interval)
			}
		}

		select {
		case <-s.quit:
			return
		case <-t.C:
			continue
		}
	}
}

func (s *star) Start() {
	log.Info("Starting star monitor")
	go s.monitor()
}

func (s *star) Stop() {
	log.Info("Stopping star monitor")
	s.quit <- struct{}{}
}
