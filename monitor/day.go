package monitor

import (
	"fmt"
	"html"
	"time"

	"github.com/cenkalti/backoff/v4"
	log "github.com/sirupsen/logrus"
	"gitlab.com/dmbb/AoCSlack/advent"
	"gitlab.com/dmbb/AoCSlack/cfg"
	"gitlab.com/dmbb/AoCSlack/slack"
)

type Day interface {
	Start()
	Stop()
}

func NewDay(config cfg.Config, sc slack.Client, ac advent.Client) (Day, chan struct{}) {
	d := &day{config: config, sc: sc, ac: ac}
	d.quit = make(chan struct{})
	return d, make(chan struct{})
}

type day struct {
	config cfg.Config
	sc     slack.Client
	ac     advent.Client

	quit chan struct{}
}

const christmasTreeEmoji = ":christmas_tree:"

func (d *day) sendDayInformation(t time.Time) error {
	l, err := d.ac.GetLeaderboard(t.Year())
	if err != nil {
		return fmt.Errorf("failed to fetch leaderboard: %s", err.Error())
	}

	title, content, url, err := d.ac.GetDay(t.Day(), t.Year())
	if err != nil {
		return fmt.Errorf("failed to fetch day information from %v: %s", url, err.Error())
	}

	b := slack.NewBlock()
	b.AddURLBlock(christmasTreeEmoji+title+christmasTreeEmoji, url)
	b.AddTextBlock(html.UnescapeString(content))
	b.AddLeaderboardBlock(l.GetMembers())
	b.AddURL(d.config.GetLeaderboardUrl(t.Year()))

	log.Infof("Sending update: %v", b)
	err = d.sc.Send(b)
	if err != nil {
		return fmt.Errorf("failed to send daily update: %s", err.Error())
	}
	return nil
}

func (d *day) getUpdateFunc() func() error {
	return func() error {
		err := d.sendDayInformation(time.Now())
		if err != nil {
			log.Errorf("failed to send day information: %v", err.Error())
		}
		return err
	}
}

const dayInterval = 24 * time.Hour

func (d *day) delay() time.Duration {
	now := time.Now()

	start := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 2, 0, now.Location())
	if now.Month() != time.December {
		start = time.Date(now.Year(), time.December, 1, 0, 0, 2, 0, now.Location())
	}

	if now.After(start) {
		start = start.Add(dayInterval)
	}

	log.Infof("Waiting %v for the next day to unlock", start.Sub(now))
	return start.Sub(now)
}

func getBackoffConfig() *backoff.ExponentialBackOff {
	b := backoff.NewExponentialBackOff()
	b.MaxInterval = 10 * time.Minute
	b.MaxElapsedTime = 12 * time.Hour
	return b
}

func (d *day) monitor() {
	updateFunc := d.getUpdateFunc()
	b := getBackoffConfig()
	t := time.NewTicker(dayInterval) // Temporary just for the declaration

	for {
		t.Reset(d.delay())

		select {
		case <-d.quit:
			return
		case <-t.C:
			err := backoff.Retry(updateFunc, b)
			if err != nil {
				log.Errorf("failed to send day information: %v", err.Error())
			}
		}
	}
}

func (d *day) Start() {
	go d.monitor()
}

func (d *day) Stop() {
	d.quit <- struct{}{}
}
